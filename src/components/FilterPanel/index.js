import './index.css';

export default function FilterPanel({onFilterTypeChanged, activeFilter}) {

    return (
        <div className='filter-panel-div'>
            <div className="btn-group" role="group" aria-label="Filter panel">
                <button type="button"
                        className={`btn btn-${activeFilter !== "all" ? "outline-secondary" : "primary"}`}
                        onClick={()=> onFilterTypeChanged('all')}>
                    All
                </button>
                <button type="button"
                        className={`btn btn-${activeFilter !== "active" ? "outline-secondary" : "success"}`}
                        onClick={()=> onFilterTypeChanged('active')}>
                    Active
                </button>
                <button type="button"
                        className={`btn btn-${activeFilter !== "done" ? "outline-secondary" : "danger"}`}
                        onClick={()=> onFilterTypeChanged('done')}>
                    Done
                </button>
            </div>
        </div>);
}