import React, {Component} from 'react';
import { Input, Button } from 'antd';
import './index.css';

class AddTask extends Component {
    state = {
        nameTask: ''
    };

    onSubmit = (e) => {
        e.preventDefault();
        this.props.onAdded(this.state.nameTask);
        this.setState({ nameTask: ''});
    }

    render () {
        return (
            <form onSubmit={(e) => this.onSubmit(e)}>
                <div className="input-group add-task-div">
                    <Input style={{ marginRight: '10px' }} className="form-control" id="inputTask" name="inputTask"
                           placeholder='Name task' maxLength="25"
                           onChange={(e) => this.setState({nameTask: e.target.value})}
                           value={this.state.nameTask}
                           required />

                    <Button htmlType="submit" className="btn btn-success">
                        Add
                    </Button>
                </div>
            </form>);
    }
}

export default AddTask;