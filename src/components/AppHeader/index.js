import React from 'react';
import { UnorderedListOutlined } from '@ant-design/icons';
import './index.css';

export default function AppHeader({toDo, done}) {
    return (
        <div className="app-header d-flex">
            <h1>
                <UnorderedListOutlined twoToneColor="#52c41a"/>
                &nbsp;My todo  list
            </h1>
            <h2>{toDo} more to do, {done} done</h2>
        </div>);
}