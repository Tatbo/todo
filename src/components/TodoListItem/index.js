import React from 'react';
import { Button, Popover, Popconfirm } from 'antd';
import './index.css';

const TodoListItem = ({name, isImportant, isDone, onToggleImportant, onToggleDone, onDeleted}) => {

    const className = `${isImportant ? ' important' : ''} ${isDone ? ' done' : ''}`;

    return (
        <div className = 'container'>
            <div className="row">
                <Popover content={`Click for ${isDone ? "active" : "done"}`} title={`Set ${isDone ? "active" : "done"}`}>
                    <div className={`col-10 todo-list-item ${className}`} onClick={ onToggleDone }>
                        {name}
                    </div>
                </Popover>

                <div className="col-1">

                    <Button className={`btn btn-${!isImportant ? 'outline-' : ''}primary btn-sm todo-list-button`}
                            title={!isImportant ? 'Set important' : 'Unset important'}
                            onClick={ onToggleImportant }>
                        <i className="fa fa-exclamation"/>
                    </Button>

                </div>

                <div className="col-1">
                    <Popconfirm
                        title="Delete the task"
                        description="Are you sure to delete this task?"
                        onConfirm={ onDeleted }
                        okText="Yes"
                        cancelText="No">

                        <Button className={`btn btn-outline-danger btn-sm todo-list-button`}
                                title={'Delete task'}>
                            <i className="fa fa-trash"/>
                        </Button>

                    </Popconfirm>
                </div>
            </div>
        </div>);

}

export default TodoListItem;