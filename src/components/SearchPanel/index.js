import { Input } from 'antd';
import './index.css';

const SearchPanel = ({onSearchTextChanged}) => {
    return (
        <Input className='input-search' placeholder='search text '
               allowClear maxLength={25}
               onChange={(e) => onSearchTextChanged(e.target.value)}
        />
    )
}

export default SearchPanel;