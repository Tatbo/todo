import React,{Component}  from 'react';
import AppHeader from '../AppHeader';
import SearchPanel from '../FilterPanel';
import FilterPanel from '../FilterPanel';
import TodoList from '../TodoList';
import AddTask from '../AddTask';
import './index.css';

class App extends Component {
  state = {
    todoData: [
      { id: 1, name: 'Drink Coffee', isImportant: false, isDone: true },
      { id: 2, name: 'Backend development', isImportant: true, isDone: false },
      { id: 3, name: 'Have lunch', isImportant: false, isDone: false },
      { id: 4, name: 'Build React App', isImportant: true, isDone: false },
      { id: 5, name: 'Have supper', isImportant: false, isDone: false },
      { id: 6, name: 'Watch movie', isImportant: false, isDone: false }],
    displayData: [],
    searchText: '',
    filter: 'all'
  };

  deletedItem = (id) => {
    this.setState(({ todoData }) => {
      const idx = todoData.findIndex((el) => el.id === id);
      const beforeArray = todoData.slice(0, idx);
      const afterArray = todoData.slice(idx+1);
      const newArray = [ ...beforeArray, ...afterArray ];

      return {
        todoData: newArray
      }
    });
  }

  addedItem = (name) => {
    this.setState(({ todoData }) => {
      const maxValue = todoData.reduce((p, v) =>
          p.id > v.id ? p.id : v.id, 0 );

      return {
        todoData: [ ...todoData,
          {id: maxValue + 1, name, isImportant: false, isDone: false}]
      }
    });
  }

  onToggleImportant = (id) => {
    this.setState(({ todoData }) => this.onToggleProperty(todoData, id, "isImportant"))
  }

  onToggleDone = (id) => {
    this.setState(({ todoData }) => this.onToggleProperty(todoData, id, "isDone"))
  }

  onToggleProperty = (arr, id, propName) => {
    const idx = arr.findIndex((el) => el.id === id);
    const oldItem = arr[idx];
    const newItem = {...oldItem, [propName]: !oldItem[propName]};
    const newArray = [ ...arr.slice(0, idx), newItem, ...arr.slice(idx+1) ];

    return {
      todoData: newArray
    };
  };

  setSearchValue = (searchText) => {
    this.setState({searchText});
  };

  setFilterType = (filter) => {
    this.setState({filter});
  }

  componentDidMount() {
    this.setState(({todoData}) => { return {displayData: [...todoData]}; });
  }

  componentDidUpdate(prevProps, prevState) {
    const {todoData, searchText, filter} = this.state;
    if (prevState.todoData !== todoData || prevState.filter !== filter || prevState.searchText !== searchText) {
      this.setState(({todoData}) => {
        const  newArray = [...todoData]
            .filter(item =>
                (item.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1)
                &&
                (filter === 'active'
                    ? !item.isDone
                    : (filter === 'done' ? item.isDone : true))
            );

        return {
          displayData: newArray
        }
      })
    }
  }

  render () {

    const doneCount = this.state.displayData.filter(item => item.isDone).length;
    const todoCount = this.state.displayData.length - doneCount;
    return (
        <div className={'app'}>
          <div className={'app-inner'}>
            <AppHeader toDo={todoCount} done={doneCount}/>
            <SearchPanel onSearchTextChanged={this.setSearchValue}/>
            <FilterPanel
                activeFilter={this.state.filter}
                onFilterTypeChanged={this.setFilterType} />
            <TodoList todos={this.state.displayData}
                      onDeleted={this.deletedItem}
                      onToggleImportant={this.onToggleImportant}
                      onToggleDone={this.onToggleDone} />
            <AddTask onAdded={this.addedItem} />
          </div>
        </div>);
  }
}

export default App;