import { List } from 'antd';

import TodoListItem from "../TodoListItem";
import './index.css';

const TodoList = ({todos, onDeleted, onToggleImportant, onToggleDone}) => {
    return (
        <List size="small"
              bordered
              dataSource={todos}
              renderItem={({ id, ...itemsProp}) => (
                  <List.Item>
                      <TodoListItem {...itemsProp}
                                    onDeleted={ () => onDeleted(id) }
                                    onToggleImportant={() => onToggleImportant(id)}
                                    onToggleDone={() => onToggleDone(id)}
                      />
                  </List.Item>
              )}
        />);
};

export default TodoList;